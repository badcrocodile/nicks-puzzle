# Nicks Puzzle

* Construct an eight digit number. Here are the rules to construct the number-  
* The digit on the units place signifies the number of 7s in the number.
* The digit in the tens place signifies the number of 6s in the number.
* The digit in hundreds place signifies the number of 5s in the number.
* And So On - all the way to – the first digit in the number signifies the number of 0s in the number.
